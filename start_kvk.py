#!/usr/bin/env python
##
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from urllib.parse import urlparse, parse_qs
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString
import re
import time
from io import StringIO
import pandas as pd
from datetime import date
from peewee import *
from database.tortugacapital import Kvk
from database.tortugacapital import Kadaster, KadasterOwnerInfo

class kvk_scraper:
    def __init__(self):
        self.s = requests.Session()
        print('starting kvkscraper...')
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        s = None

        # main function
    def _parse_kvk_page(self, query, name):
        result_dict = {}
        #sets the headers that are accepted by kvk
        headers = {
            'Accept': '*/*',
            'Origin'    :   'https://www.kvk.nl',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'zoeken.kvk.nl',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15',
            'Accept-Language': 'en-us',
            'Connection': 'keep-alive',
            }

        #gives parameters to the kvk search function
        params = {
            'source'  : 'all',
            'q' : query,
            'start'   : '0',
            'site'    : 'kvk2014',
            '_' :   '1546952848356',
            'index' :   '4'
            }

        try:
            response = requests.get('https://zoeken.kvk.nl/search.ashx', params=params, headers=headers).text
            time.sleep(5)
            #parse kvk number that we got from response
            kvk_nr = self._get_kvk_nr_from_element(response)
            #parse address that we got from response
            address = self._get_address_from_element(response)
            #update result dict we the results that we just parsed and add the original company name
            result_dict.update({
                            'company'   :   name,
                            'kvk_nr'  : kvk_nr,
                            'street_name'    :   address.get('Street_name'),
                            'zip_code'  :   address.get('Zip_code'),
                            'city'  :   address.get('City')
                            })
            return result_dict
        except Exception as e:
            print(e)
            pass

    #parses kvk number
    def _get_kvk_nr_from_element(self, response):
        response = str(response)
        signal_start = 'href="/orderstraat/product-kiezen/?kvknummer='
        signal_end = '">'
        try:
            html2 = response.split(signal_start)
            result = html2[1].split(signal_end)
            return result[0]
        except IndexError:
            pass

            #parses address
    def _get_address_from_element(self, response):
        result_dict = {}
        webpage_soup = BeautifulSoup(response, 'html.parser')
        try:
            content_table = webpage_soup.find('ul', class_='kvk-meta').findAll('li')
                #print(content_table)
            if content_table:
                result_dict.update({
                                    'Street_name'   :   content_table[2].get_text(),
                                    'Zip_code'  :   content_table[3].get_text(),
                                    'City'  :   content_table[4].get_text()
                                    })
                return result_dict
                print(result_dict)
            else:
                pass

        except AttributeError:
            pass

    def kvkscraper(self, owner):
        if owner is not None:
                if owner['natuurlijke_persoon_code'] is None:
                    bv_info = owner['owner_full_name']
                    try:
                        query = bv_info.replace(" ","%").lower()
                        # print("Searching for: " + query)
                        result = self._parse_kvk_page(query, bv_info)
                        # print(result)
                        if result is not None and result.get('kvk_nr') is not None:
                            # print(result)
                            result.update({
                                'owner_persoonsnummer'  :   owner['owner_persoonsnummer']
                            })
                            #check for duplicate
                            q_duplicate = (Kvk
                                            .select(Kvk.company)
                                            .where(Kvk.company == result.get('company'))
                                            .dicts())

                            if len(q_duplicate) > 0:
                                #if we found a duplicate, update kvk table
                                print('Updating org')
                                print(type(result.get('city')))
                                print(type(result.get('kvk_nr')))
                                print(type(result.get('street_name')))
                                print(type(result.get('zip_code')))
                                print(type(result.get('owner_persoonsnummer')))
                                print(type(result.get('company')))

                                q_update = (Kvk
                                            .update(city = result.get('city'),
                                                    kvks = result.get('kvk_nr'),
                                                    street_name = result.get('street_name'),
                                                    zip_code = result.get('zip_code'),
                                                    date = date,
                                                    kadaster_id = result.get('owner_persoonsnummer'),
                                                    script_version = '4')
                                            .where(Kvk.company == result.get('company'))
                                            .execute())
                            else:
                                #if we didn't find a duplicate, insert in kvk table
                                print('Adding org')
                                q_insert = (Kvk
                                            .insert(city = result.get('city'),
                                                    company = result.get('company'),
                                                    kvks = result.get('kvk_nr'),
                                                    street_name = result.get('street_name'),
                                                    zip_code = result.get('zip_code'),
                                                    date = date,
                                                    kadaster_id = result.get('owner_persoonsnummer'),
                                                    script_version = '4')
                                            .execute())
                        else:
                            pass

                    except Exception as e:
                        print(e)
                        pass
