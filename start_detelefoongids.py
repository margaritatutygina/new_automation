import requests
from bs4 import BeautifulSoup
from peewee import *
from database.tortugacapital import Kadaster, Detelefoongids
from datetime import date
import json

date = date.today()
# owner_voorletters = None
# owner_voorvoegsel = None
# owner_voornaam = None
# owner_naam = None
# owner_row_owner = None
url = 'https://www.detelefoongids.nl/v1/rest/api/wpSearchResult'


## for testing purposes
# owner = [{
#         'owner_voorletters' : 'C.S.',
#         'owner_row_owner' : 'CAROLINE SUSANNE VAN DE GRIEND',
#         'woonplaats' : 'Rotterdam',
#         'owner_voorvoegsel' : 'VAN DE',
#         'owner_voornaam' : 'CAROLINE SUSANNE',
#         'owner_persoonsnummer' : '158108377',
#         'owner_naam' : 'GRIEND',
#         'zipcode' : '3023GL',
#         'object_raw_street_plus_number' : 'Mathenesserlaan 483 A',
#         'object_raw_zipcode_plus_city' : '3023GL Rotterdam',
#         }]

class detelefoongids_scraper:
    def detelefoongidsscraper(self, owner):
        print(owner)
        exit()
        print('starting detelefoongidsscraper...')
            # print(owner['owner_row_owner'])
            # if "b.v." or "BV" or "B.V." or "bv" in owner['owner_row_owner']:
            #     owner_full_name = owner['owner_row_owner']
            #     print('yes')
            # else:
            #
            #     if owner['owner_naam'] is not None:
            #         owner_naam = owner['owner_naam']
            #         owner_naam = owner_naam[0] + owner_naam[1:].lower()
            #     if owner['owner_voorletters'] is not None:
            #         owner_voorletters = owner['owner_voorletters'].replace("."," ")
            #     if owner['owner_voorvoegsel'] is not None:
            #         owner_voorvoegsel = owner['owner_voorvoegsel']
            #         if owner_voorvoegsel == "VAN DE":
            #             owner_voorvoegsel = 'vd'
            #     if owner['owner_voornaam'] is not None:
            #         owner_voornaam = owner['owner_voornaam']
            #     if owner['owner_row_owner'] is not None:
            #         owner_row_owner = owner['owner_row_owner']
            #
            #
            #     if owner_naam is not None and owner_voorletters is not None or owner_voornaam is not None and owner_voorvoegsel is not None:
            #         try:
            #             owner_full_name = owner_naam + " " + owner_voorletters + " " + owner_voorvoegsel
            #         except:
            #             owner_full_name = owner_naam + " " + owner_voornaam + " " + owner_voorvoegsel
            #
            #     elif owner_naam is not None and owner_voorletters is not None or owner_voornaam is not None:
            #         try:
            #             owner_full_name = owner_naam + " " + owner_voorletters
            #         except:
            #             owner_full_name = owner_naam + " " + owner_voornaam
            #
            #     elif owner_naam is not None:
            #         owner_full_name = owner_naam
            #
            #     else:
            #         owner_full_naam = owner_row_owner
            # print(counter)
            # print(owner_full_name)

        req = requests.Session()
        data = {
            "headers": {},
            "originPath": "",
            "what": owner['owner_full_name'],
            "whereTerm": {
                "lat": None,
                "lng": None,
                "locationCode": None
            },
            "originalWhat": "",
            "originalWhere": "",
            "collapsing": True,
            "debug": "",
            "debugPass": "",
            "encodedRefinement": "",
            "deletedListingId": None,
            "sortBy": "relevance",
            "limit": 100000,
            "startIndex": 0,
            "fromHomeCategoryClick": False
        }

        res = req.post(url, json = data)
        parsed_data = json.loads(res.text)
        # print(parsed_data)

        for result in parsed_data['results']:
            zipcode_from_website = result['address']['zipCode']
            city_from_website = result['address']['city']['name']
            # print(city_from_website)
            # print(zipcode_from_website)
            full_zipcode_plus_city = zipcode_from_website + " " + city_from_website
            print('parsed from internet: ' + full_zipcode_plus_city)
            print('from database: ' + owner['owner_zipcode_plus_city'])
            # name_from_website = result['name']
            # if owner['zipcode'] == zipcode_from_website or owner_full_name == name_from_website:
            if owner['owner_zipcode_plus_city'] == zipcode_from_website:
                second_url = 'https://www.detelefoongids.nl' + result['_links']['DETAILS']['href']

                resp_2 = req.get(second_url).text
                soup = BeautifulSoup(resp_2, 'html.parser')
                try:
                    phone_number_div = soup.find("div", {"id": "detail-phone-button-additional"})
                    phone_number = phone_number_div.contents[0].contents[0]
                    print(phone_number)
                except:
                    pass



                # select_duplicate = Detelefoongids.select(Detelefoongids.owner_name).where(Detelefoongids.kadaster_id == owner['owner_persoonsnummer'])
                # if len(select_duplicate) > 0:
                #     print('Updating Detelefoongids')
                #     q_update = (Detelefoongids
                #                 .update(
                #                     owner_name = owner['owner_row_owner'],
                #                     phone_number = phone_number,
                #                     date = date,
                #                     kadaster_id = owner['owner_persoonsnummer'],
                #                     script_version = '2')
                #                 .where(Detelefoongids.kadaster_id == owner['owner_persoonsnummer'])
                #                 .execute())
                # else:
                #     print('Inserting in Detelefoongids')
                #     q_update = (Detelefoongids
                #                 .insert(
                #                     owner_name = owner['owner_row_owner'],
                #                     phone_number = phone_number,
                #                     date = date,
                #                     kadaster_id = owner['owner_persoonsnummer'],
                #                     script_version = '2')
                #                 .execute())
        # break
