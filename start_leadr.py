import glob
import pandas as pd
import requests
import time
from pandas.io.json import json_normalize


class Leadr_scraper:
    def leadrscraper():
        print('starting leadrscraper...')
        headers = {
        'Accept': 'application/json, text/plain, */*',
        'Origin': 'https://app.leadr.nl',
        'Referer': 'https://app.leadr.nl/companies/eeeb53d8-246c-11e7-9082-525400bd48f3',
        'DNT': '1',
        'Host': 'api.leadr.nl',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.2 Safari/605.1.15',
        'Accept-Language': 'nl-nl',
        'Accept-Encoding': 'br, gzip, deflate',
        'Connection': 'keep-alive',
        }
        s = None

        def __init__(self):
            self.s = requests.Session()

        def searchKvk(self, kvk):
            params = (
               ('currentPage', '0'),
               ('search', kvk),
            )
            response = requests.get('https://api.leadr.nl/api/companies', headers = self.headers, params = params)

            return response.json()['companies']
            print(response.json())

        def get_details(self, uuid):
            url = 'https://api.leadr.nl/api/companies/{}'.format(uuid)
            response = requests.get(url, headers = self.headers)
            return response.json()
