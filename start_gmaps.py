#!/usr/bin/env python
import googlemaps
import pymysql
import pandas as pd
import requests
from database.tortugacapital import KadasterOwnerInfo, Gmaps
from datetime import date

date = date.today()


class gmaps_scraper:
    def __init__(self):
        self.results = {}
        self.key = 'AIzaSyD62O9kl4X6RzG3e4Z_SE2nt0YR25xCfeA'
        self.gmaps = googlemaps.Client(key=self.key)
        self.details_url = "https://maps.googleapis.com/maps/api/place/details/json"
        ## Test whether scraper works with line below
        # owners = [{'owner_full_name' : 'Hericon Vastgoed B.V.', 'owner_persoonsnummer' : '24273127'}]
    def gmapsscraper(self, owner):
        print('starting gmapsscraper...')
        # for owner in data:
        try:
            website, address, latitude, longitude, phone = None, None, None, None, None
            owner_name = owner.get('owner_full_name')
            place = self.gmaps.find_place(input=owner_name, input_type='textquery', fields=['place_id', 'formatted_address'])
            place_id =  place['candidates'][0].get('place_id')
            details_payload = {"key":self.key, "placeid":place_id}
            details_resp = requests.get(self.details_url, params=details_payload)
            details_json = details_resp.json()

            try:
                website = details_json['result']['website']
            except:
                pass

            try:
                address = details_json['result']['formatted_address']
            except:
                pass

            try:
                latitude = details_json['result']['geometry']['location']['lat']
            except:
                pass

            try:
                longitude = details_json['result']['geometry']['location']['lng']
            except:
                pass

            try:
                phone = details_json['result']['formatted_phone_number']
                phone = phone.replace(" ", "").replace("\t", "")
            except:
                pass

            self.results.update({"website" : website,
                        "address" : address,
                        "latitude" : latitude,
                        "longitude" : longitude,
                        "phone" : phone,
                        "owner_name" : owner_name,
                        "kadaster_id" : owner.get('owner_persoonsnummer')
                        })

            self._insert_details(self.results)
            print(self.results)
            # if "drimble" in results['website']:
            #     print('Drimble found.')

        except Exception as e:
            print(e)
            pass

    def _insert_details(self, results):
        g_duplicate = Gmaps.select(Gmaps.owner_row_owner).where(Gmaps.kadaster_id == results['kadaster_id'])

        if len(g_duplicate) > 0:
            print("Updating\n")
            g_update = (Gmaps
                        .update(owner_row_owner = results['owner_name'],
                                company_address = results['address'],
                                phone_number = results['phone'],
                                website = results['website'],
                                kadaster_id = results['kadaster_id'],
                                latitude = results['latitude'],
                                longitude = results['longitude'],
                                script_date = date,
                                script_version = '1')
                        .where(Gmaps.owner_row_owner == results['owner_name'])
                        .execute())

        else:
            print("Inserting\n")
            g_insert = (Gmaps
                        .insert(owner_row_owner = results['owner_name'],
                                company_address = results['address'],
                                phone_number = results['phone'],
                                website = results['website'],
                                kadaster_id = results['kadaster_id'],
                                latitude = results['latitude'],
                                longitude = results['longitude'],
                                script_date = date,
                                script_version = '1')
                        .execute())
