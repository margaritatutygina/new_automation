import requests
from bs4 import BeautifulSoup
from database.tortugacapital import Kadaster, Detelefoongids, Telefoonboek
import json
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import date
import os


date = date.today()
url_for_np = 'https://www.telefoonboek.nl/personen/'
url_for_nnp = 'https://www.telefoonboek.nl/zoeken/'


class telefoonboek_scraper:
    def telefoonboekscraper(self, owner):
        print('starting telefoonboekscraper...')
        # for owner in owner:

        req = requests.Session()
            # try:
        owner_name = owner['owner_full_name'].lower().replace(" ", "-")
        owner_name = owner_name.replace("--", "-")
        address = owner['owner_zipcode_plus_city'].lower().replace(" ", "-")
            # except:
            #     continue

        if owner['natuurlijke_persoon_code'] is not None:
            final_url = url_for_np + owner_name + '/' + address
        else:
            final_url = url_for_nnp + owner_name + '/' + address

        res = req.get(final_url).text
        soup = BeautifulSoup(res, 'html.parser')

        is_found = soup.find("h1", {"class": "h1-with-trigger"})
        if is_found is not None:
            is_found_contents = is_found.contents

            for x in is_found_contents:
                if "niet gevonden" in x:
                    continue
        else:
            potential_candidates = soup.find("div", {"class": "description person-result"})
            postcode = potential_candidates.find("span", {"class": "postal-code"})
            cleaned_postcode = postcode.contents[0].replace("\n", "").replace(" ", "")
            if cleaned_postcode == owner['zipcode']:
                link = potential_candidates.find("a", {"class": "title-link"})['href']
                select_duplicate = Telefoonboek.select(Telefoonboek.owner_row_owner).where(Telefoonboek.kadaster_id == owner['owner_persoonsnummer'])
                print(potential_candidates)
                if len(select_duplicate) > 0:
                    print('Updating Telefoonboek')
                    q_update = (Telefoonboek
                                .update(
                                    owner_row_owner = owner['owner_row_owner'],
                                    phone_number = link,
                                    date = date,
                                    kadaster_id = owner['owner_persoonsnummer'],
                                    script_version = '4')
                                .where(Telefoonboek.kadaster_id == owner['owner_persoonsnummer'])
                                .execute())
                else:
                    print('Inserting in Telefoonboek')
                    q_update = (Telefoonboek
                                 .insert(
                                    owner_row_owner = owner['owner_row_owner'],
                                    phone_number = link,
                                    date = date,
                                    kadaster_id = owner['owner_persoonsnummer'],
                                    script_version = '4')
                                .execute())
