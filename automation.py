# from start_gmaps import gmaps_scraper
# from start_kvk import kvk_scraper
# from start_telefoonboek import telefoonboek_scraper
from start_detelefoongids import detelefoongids_scraper
from database.tortugacapital import KadasterOwnerInfo

data = (KadasterOwnerInfo.select().where(KadasterOwnerInfo.owner_zipcode_plus_city != None).distinct(KadasterOwnerInfo.owner_persoonsnummer).dicts())

# gmaps_scraper = gmaps_scraper()
# kvk_scraper = kvk_scraper()
# telefoonboek_scraper = telefoonboek_scraper()
detelefoongids_scraper = detelefoongids_scraper()

for owner in data:
    # start_detelefoongids.detelefoongids(data)
    # Leadr_scraper.leadrscraper()


    # gmaps_scraper.gmapsscraper(owner)
    # kvk_scraper.kvkscraper(owner)
    # telefoonboek_scraper.telefoonboekscraper(owner)
    detelefoongids_scraper.detelefoongidsscraper(owner)
